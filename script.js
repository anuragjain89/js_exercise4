var ParentCheckbox = function (element, childElementNames) {
  this.parentCheckbox = element;
  this.childElementNames = childElementNames;
  this.childId = element.name + 'Child';
  this.childElement;
};

ParentCheckbox.prototype.createChildCheckbox = function (labelText) {
  var checkbox = document.createElement('input');
  checkbox.type = 'checkbox';
  checkbox.name = labelText;
  checkbox.value = labelText;
  checkbox.checked = false;
  checkbox.id = labelText;
  var label = document.createElement('label');
  var text = document.createTextNode(labelText);
  label.appendChild(checkbox);
  label.appendChild(text);
  return label;
};

ParentCheckbox.prototype.getChildElement = function () {
  var docfrag = document.createDocumentFragment();
  var ul = document.createElement('ul');
  ul.id = this.childId;

  var appendListItemToUnorderedList = function (childCheckboxName) {
    var li = document.createElement('li');
    li.appendChild(this.prototype.createChildCheckbox(childCheckboxName));
    ul.appendChild(li);
  };
  this.childElementNames.forEach(appendListItemToUnorderedList, ParentCheckbox);

  docfrag.appendChild(ul);
  return docfrag;
};

ParentCheckbox.prototype.CHECKBOX_SELECTOR = 'li input[type="checkbox"]';

ParentCheckbox.prototype.setChildCheckbox = function (childElements, value) {
  var checkboxElements = Array.prototype.slice.call(childElements.querySelectorAll(ParentCheckbox.prototype.CHECKBOX_SELECTOR));
  checkboxElements.forEach(function (checkbox) {
    checkbox.checked = value;
  });
};

ParentCheckbox.prototype.getOnChangeListener = function (childElement) {
  return function () {
    if (this.checked === true) {
      ParentCheckbox.prototype.setChildCheckbox(childElement, true);
      childElement.style.display = 'block';
      childElement.querySelector(ParentCheckbox.prototype.CHECKBOX_SELECTOR).focus();
    } else {
      ParentCheckbox.prototype.setChildCheckbox(childElement, false);
      childElement.style.display = 'none';
    }
  };
};

ParentCheckbox.prototype.initChangeListeners = function () {
  var changeListener = this.getOnChangeListener(this.childElement);
  this.parentCheckbox.addEventListener('change', changeListener);
};

ParentCheckbox.prototype.init = function () {
  document.getElementById('container').appendChild(this.getChildElement());
  this.childElement = document.getElementById(this.childId);
  this.childElement.style.display = 'none';
  this.initChangeListeners();
};

var colorCheckbox = new ParentCheckbox(document.getElementById('color'), ['red', 'yellow', 'green', 'blue']);
colorCheckbox.init();
var drinksCheckbox = new ParentCheckbox(document.getElementById('drinks'), ['coke', 'pepsi', 'dew']);
drinksCheckbox.init();
var moviesCheckbox = new ParentCheckbox(document.getElementById('movies'), ['Dar', 'Sir']);
moviesCheckbox.init();
var bikesCheckbox = new ParentCheckbox(document.getElementById('bikes'), ['v-rod', 'pulsar', 'cbz']);
bikesCheckbox.init();
